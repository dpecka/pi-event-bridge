import hashlib
import json
import time

from event_handler import logger, CONFIG_CHECK_INTERVAL_SEC
from events import stop_all_events, start_event

def load_configuration(config_path):
    logger.info('Loading configuration.')
    with open(config_path, 'r') as config_file:
        config = json.loads(config_file.read())

        for event in config['events']:
            start_event(event)
    globals()['config'] = config

def monitor_configuration(config_path):
    last_known_hash = None
    while True:
        with open(config_path, 'r') as config_file:
            sha256 = hashlib.sha256()
            sha256.update(config_file.read().encode('utf-8'))
            current_hash = sha256.digest()
            if current_hash != last_known_hash:
                if last_known_hash is not None:
                    logger.info('Configuration modified, event threads will halt and be reloaded!')
                    stop_all_events()
                load_configuration(config_path)
            last_known_hash = current_hash
        time.sleep(CONFIG_CHECK_INTERVAL_SEC)
