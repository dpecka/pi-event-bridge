import logging
import sys


CONFIG_CHECK_INTERVAL_SEC = 5

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(process)d->%(thread)d %(message)s')
logger = logging.getLogger('pi-event-handler')

ACTIVE_EVENTS = list()


class EventHandler(object):
    def start(self, config_path='config.json'):
        from configuration import monitor_configuration
        from events import stop_all_events

        try:
            logger.info('Starting up. Configuration file: %s' % config_path)
            monitor_configuration(config_path)
        except KeyboardInterrupt:
            logger.warning('Caught KeyboardInterrupt. Stopping all events.')
            stop_all_events()
            logger.warning('Exiting.')


if __name__ == '__main__':
    handler = EventHandler()
    handler.start(sys.argv[1] if len(sys.argv) == 2 else 'config.json')
