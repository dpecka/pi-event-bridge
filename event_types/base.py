import logging
import importlib

from interpolation import interpolate_recursive


class BaseEventType(object):
    def __init__(self, *args, **kwargs):
        self._stop = False
        self.logger = logging.getLogger('event_types.%s' % self.__class__.__name__)
        self.logger.debug('Creating EventType[%s]' % self.__class__.__name__)

    def start(self, *args, **kwargs):
        raise Exception("EventType[%s].start() not yet implemented!" % self.__class__.__name__)

    def stop(self, *args, **kwargs):
        self._stop = True

    def trigger(self):
        self.logger.info('EventType[%s]:"%s" Triggered' % (self.__class__.__name__, self.definition['name']))
        for reaction in self.definition['reactions']:
            module_name = str(reaction['type']).split('.')[0]
            class_name = str(reaction['type']).split('.')[1]
            spec = importlib.util.spec_from_file_location("reaction_types.%s" % module_name,
                                                          "reaction_types/%s.py" % module_name)
            globals()[module_name] = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(globals()[module_name])

            reaction_instance = getattr(globals()[module_name], class_name)()
            reaction_instance.event = self

            reaction_instance.run(**(interpolate_recursive(reaction_instance, reaction['args'])) if 'args' in reaction else dict())

class ThreadedEventType(BaseEventType):
    def stop(self, *args, **kwargs):
        self._stop = True
