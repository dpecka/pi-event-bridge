from event_types.base import ThreadedEventType
import RPi.GPIO as GPIO


class GPIOInput(ThreadedEventType):
    def _change_callback(self, pin):
        # todo: set self.state to on/off state of pin!
        self.state = 'on' if GPIO.input(pin) else 'off'
        self.trigger()

    def start(self, pin=None):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(pin, GPIO.BOTH, callback=self._change_callback)
