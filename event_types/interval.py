import time
import datetime
from event_types.base import ThreadedEventType


class IntervalEvent(ThreadedEventType):
    def start(self, interval_seconds=5):
        while True and not self._stop:
            self.last_trigger_time = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
            self.trigger()
            time.sleep(interval_seconds)
