import hashlib
import json
import importlib.util
from threading import Thread

from event_handler import logger, ACTIVE_EVENTS


def start_event(event_definition):
    logger.info('Adding EventType[%s]:"%s"' % (event_definition['type'], event_definition['name']))

    sha256 = hashlib.sha256()
    sha256.update(json.dumps(event_definition).encode('utf-8'))

    module_name = str(event_definition['type']).split('.')[0]
    class_name = str(event_definition['type']).split('.')[1]
    spec = importlib.util.spec_from_file_location(
        "event_types.%s" % module_name, "event_types/%s.py" % module_name)
    globals()[module_name] = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(globals()[module_name])

    instance = getattr(globals()[module_name], class_name)(**event_definition['args'])
    instance.definition = event_definition
    instance.thread = Thread(target=instance.start, kwargs=event_definition['args'])
    instance.hash = sha256.hexdigest()
    instance.module = globals()[module_name]

    instance.thread.start()

    ACTIVE_EVENTS.append(instance)


def stop_event(event_name=None, event_definition_hash=None):
    events_to_stop = [e for e in ACTIVE_EVENTS if e.definition['name'] == event_name or e.hash == event_definition_hash]
    for event in events_to_stop:
        logger.info('Stopping EventType[%s]:"%s"' % (event.__class__.__name__, event.definition['name']))
        event.stop()


def stop_all_events():
    logger.warning('Stopping all active event threads!')
    for event in ACTIVE_EVENTS:
        stop_event(event_definition_hash=event.hash)
