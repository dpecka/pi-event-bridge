from functools import reduce
import re


VAR_WRAP_REGEX_START = '\${'
VAR_WRAP_REGEX_END = '}'
VAR_WRAP_REPLACE_START = '${'
VAR_WRAP_REPLACE_END = '}'


def deep_getattr(obj, attr):
    return reduce(getattr, attr.split('.'), obj)

def interpolate_str(o, s):
    for var in re.findall('%s(.*?)%s' % (VAR_WRAP_REGEX_START, VAR_WRAP_REGEX_END), s):
        replacement = deep_getattr(o, var)
        s = s.replace('%s%s%s' % (VAR_WRAP_REPLACE_START, var, VAR_WRAP_REPLACE_END), replacement)

    return s

def interpolate_recursive(o, d):
    for k in d:
        if isinstance(d[k], dict) or isinstance(d[k], list):
            d[k] = interpolate_recursive(d[k])
        elif isinstance(d[k], str):
            d[k] = interpolate_str(o, d[k])
    return d
