import logging


class BaseReaction(object):
    def __init__(self, *args, **kwargs):
        self.logger = logging.getLogger('reaction_types.%s' % self.__class__.__name__)
        self.logger.debug('Creating ReactionType[%s]' % self.__class__.__name__)

    def trigger(self):
        self.logger.info('ReactionType[%s] triggered by "%s"' % (self.__class__.__name__, self.event.definition['name']))
        self.run()

    def run(self, *args, **kwargs):
        raise Exception("ReactionType[%s].run() not yet implemented!" % self.__class__.__name__)
