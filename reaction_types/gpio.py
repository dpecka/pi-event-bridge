import time
import RPi.GPIO as GPIO

from reaction_types.base import BaseReaction


class GPIOOutput(BaseReaction):
    def run(self, pin=None, duration=None):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, True)
        if duration:
            time.sleep(duration)
            GPIO.output(pin, False)
