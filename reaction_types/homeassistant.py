import urllib.request
import json

from reaction_types.base import BaseReaction


class SetEntityStateReaction(BaseReaction):
    def run(self, host, password, entity, value='X'):
        url = '%s/api/states/%s' % (host, entity)
        data = json.dumps({
            'state': value,
            'attributes': {}
        })
        headers = {
            'x-ha-access': password
        }

        req = urllib.request.Request(url, data=data.encode('utf-8'), headers=headers)
        response = urllib.request.urlopen(req)

        if 200 > response.status >= 300:
            self.event.logger.warning('ReactionType[SetEntityStateReaction] returned non-200 response: %d' % response.status)


class FireEventReaction(BaseReaction):
    def run(self, host, password, event):
        url = '%s/api/events/%s' % (host, event)
        headers = {
            'x-ha-access': password
        }

        req = urllib.request.Request(url, headers=headers)
        response = urllib.request.urlopen(req)

        if 200 > response.status >= 300:
            self.event.logger.warning('ReactionType[FireEventReaction] returned non-200 response: %d' % response.status)
