from reaction_types.base import BaseReaction


class LogReaction(BaseReaction):
    def run(self, message='LogReaction Fired!'):
        self.event.logger.info(message)

